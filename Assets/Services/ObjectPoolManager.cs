using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Services.ObjectPool
{
    public class ObjectPoolManager<T> where T : MonoBehaviour
    {
        private T m_Prefab;
        private bool m_IsAutoExpand;
        private Transform m_Container;
        private List<T> m_Pool;

        public ObjectPoolManager(T prefab, int count, bool autoExpand)
        {
            m_Prefab = prefab;
            m_Container = null;
            m_IsAutoExpand = autoExpand;

            CreatePool(prefab, count);
        }

        public ObjectPoolManager(T prefab, int count, bool autoExpand, Transform container)
        {
            m_Prefab = prefab;
            m_Container = container;
            m_IsAutoExpand = autoExpand;

            CreatePool(prefab, count);
        }

        /// <summary>
        /// integer key in Dictionary is count of poolObjects
        /// </summary>
        /// <param name="dictionary"></param>
        /// <param name="autoExpand"></param>
        /// <param name="container"></param>
        public ObjectPoolManager(Dictionary<T, int> dictionary, bool autoExpand, Transform container)
        {
            m_Container = container;
            m_IsAutoExpand = autoExpand;

            for(int i = 0; i < dictionary.Count; i++)
            {
                CreatePool(dictionary.Keys.ToList()[i], dictionary.Values.ToList()[i]);
            }
        }

        private void CreatePool(T prefab, int count)
        {
            m_Pool = new List<T>();

            for (int i = 0; i < count; i++)
            {
                CreateObject(prefab);
            }
        }

        private T CreateObject(T prefab, bool isActiveByDefault = false)
        {
            var createdObj = Object.Instantiate(prefab, m_Container);
            createdObj.gameObject.SetActive(isActiveByDefault);
            m_Pool.Add(createdObj);
            return createdObj;
        }

        private bool HasFreeElement(out T element)
        {
            foreach (var obj in m_Pool)
            {
                if (!obj.gameObject.activeInHierarchy)
                {
                    element = obj;
                    obj.gameObject.SetActive(true);
                    return true;
                }
            }

            element = null;
            return false;
        }

        public T GetFreeElement()
        {
            if (HasFreeElement(out var element))
            {
                return element;
            }

            if (m_IsAutoExpand)
            {
                if(m_Prefab != null)
                {
                    throw new System.Exception($"There is null prefab of type {typeof(T)}");
                }
                return CreateObject(m_Prefab, true);
            }

            throw new System.Exception($"There is no free elements in pool of type {typeof(T)}");
        }
    }
}