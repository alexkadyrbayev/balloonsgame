using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Services
{
    public interface IAssetManager
    {
        public AsyncOperationHandle<T> LoadAsset<T>(object key);
        public AsyncOperationHandle Instantinate<T>(object key, Transform parent, out T result);
    }
}