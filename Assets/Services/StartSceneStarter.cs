using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartSceneStarter : MonoBehaviour
{
    [SerializeField] private Button m_PlayButton;

    private void Start()
    {
        m_PlayButton.onClick.AddListener(OnPlayButtonClick);
    }

    private void OnPlayButtonClick()
    {
        SceneManager.LoadScene("CoreScene");
    }
}
