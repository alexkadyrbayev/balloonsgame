using Newtonsoft.Json.Linq;
using System;
using System.IO;
using UnityEngine;

namespace AlexKadyrbayev.Plugins.DataManagement
{
    public class FileDataLoader : IDataLoader
    {
        private readonly string m_DefaultFileName;
        private string DefaultFilePath => Path.Combine(Application.persistentDataPath, m_DefaultFileName);
        public bool AnySaveFile => File.Exists(DefaultFilePath);

        public FileDataLoader(string fileName)
        {
            m_DefaultFileName = fileName + ".json";
        }

        public void Load(Action<JObject, bool> onLoaded)
        {
            if (AnySaveFile)
            {
                string json = File.ReadAllText(DefaultFilePath);
                JObject data = JObject.Parse(json);
                onLoaded?.Invoke(data, true);
            }
            else
            {
                onLoaded?.Invoke(null, false);
            }
        }

        public void Save(JObject jObj, Action<bool> onCompleted)
        {
            if (jObj.HasValues)
            {
                File.WriteAllText(DefaultFilePath, jObj.ToString());
                onCompleted?.Invoke(true);
                return;
            }

            onCompleted?.Invoke(false);
        }
    }
}