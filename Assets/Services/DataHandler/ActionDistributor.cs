using System;
using System.Threading.Tasks;

namespace AlexKadyrbayev.Plugins.DataManagement
{
    public class ActionDistributor
    {
        private TimeSpan m_Delay;
        private Task<int> m_CurrentTask;
        private Action m_ExecutableAction;
        private int m_BlockedRequestsCount = 0;
        private bool m_IsExecuting;
        public event Action<int> OnExecutionCompleted;

        public void DefineMarginalDelayBetweenActions(TimeSpan delay)
        {
            m_Delay = delay;
        }

        public void DefineExecutableAction(Action action)
        {
            m_ExecutableAction = action;
        }

        public async void Execute()
        {
            if (m_IsExecuting)
            {
                m_BlockedRequestsCount++;
                return;
            }

            m_IsExecuting = true;
            m_ExecutableAction?.Invoke();

            var delay = Task.Delay(m_Delay);
            await delay;

            OnExecutionCompleted?.Invoke(m_BlockedRequestsCount);
            m_IsExecuting = false;

            if (m_BlockedRequestsCount <= 0)
            {
                return;
            }

            m_BlockedRequestsCount = 0;
            Execute();
        }

        public void ExecuteForced()
        {
            m_ExecutableAction?.Invoke();
        }
    }
}