using AlexKadyrbayev.Plugins.DataHandler;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace AlexKadyrbayev.Plugins.DataManagement
{
    public class DataHandlersRegistry : IDataHandlersRegistry
    {
        private readonly Dictionary<string, IDataHandler> m_DataHandlersMap
            = new Dictionary<string, IDataHandler>();
        private bool m_Initialized;
        public IDataHandler GetDataHandlerObject(string dataKey)
        {
            LazyInitialize();
            return m_DataHandlersMap.TryGetValue(dataKey, out var dataHandlerBase)
                ? dataHandlerBase : null;
        }
        public IDataHandler<T> GetDataHandlerObject<T>(string dataKey)
        {
            LazyInitialize();
            var dataHandlerObject = GetDataHandlerObject(dataKey);
            return dataHandlerObject switch
            {
                null => throw new NullReferenceException(
                    $"Can't get DataHandler object for data key {dataKey} and type {typeof(T)}"),
                IDataHandler<T> dataHandler => dataHandler,
                _ => throw new InvalidCastException($"Can't cast {dataHandlerObject} to type {typeof(IDataHandler<T>)}")
            };
        }
        private void LazyInitialize()
        {
            if (m_Initialized)
            {
                return;
            }
            m_Initialized = true;
            var dataHandlersCollection
                = Resources.Load<DataHandlersCollection>("DataHandlersCollection");
            foreach (var dataHandlerBase in dataHandlersCollection.GetAll())
            {
                if (dataHandlerBase != null)
                {
                    m_DataHandlersMap[dataHandlerBase.GetDataKey()] = dataHandlerBase;
                }
            }
        }
    }
}