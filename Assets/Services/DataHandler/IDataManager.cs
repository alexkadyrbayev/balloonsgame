using System;

namespace AlexKadyrbayev.Plugins.DataManagement
{
    public interface IDataManager
    {
        event Action<string, object> OnDataSaved;

        /// <summary>
        /// ���������� ��� ��������� ������, 
        /// </summary>
        Action OnDataChanged { get; set; }

        /// <summary>
        /// ����������� ��� �������� ������
        /// </summary>
        Action OnDataLoaded { get; set; }

        /// <summary>
        /// ���������� �� ������
        /// </summary>
        bool IsDataLoaded { get; }

        /// <summary>
        /// ��������� ����� ������ �� ����� ���� ���� ��� ���������
        /// </summary>
        /// <typeparam name="T">��� ������������� ������</typeparam>
        /// <param name="key">���� �������� � ��������</param>
        /// <param name="data">������������� ������</param>
        /// <returns>bool</returns>
        bool TrySaveData<T>(string key, T data);

        /// <summary>
        /// ��������� ����� ������ �� ����� ���� ���� ��� ���������
        /// </summary>
        /// <param name="key">���� �������� � ��������</param>
        /// <param name="data">>������������� ������</param>
        /// <returns>bool</returns>
        bool TrySaveData(string key, object data);

        /// <summary>
        /// �������� ������ �� �����
        /// </summary>
        /// <param name="key">���� �������� � ��������</param>
        /// <param name="type">��� ������������� ������</param>
        /// <param name="data">��������������� ������</param>
        /// <returns>true ���� ������� false ���� �� ������</returns>
        bool TryGetData(string key, Type type, out object data);

        /// <summary>
        /// �������� ������ �� �����
        /// </summary>
        /// <typeparam name="T">��� ������������� ������</typeparam>
        /// <param name="key">���� �������� � ��������</param>
        /// <param name="data">��������������� ������</param>
        /// <returns>true ���� ������� false ���� �� ������</returns>
        bool TryGetData<T>(string key, out T data);

        /// <summary>
        /// ������� ������ �� �����
        /// </summary>
        /// <param name="key">���� �������� � ��������</param>
        /// <returns></returns>
        bool TryRemoveData(string key);

        /// <summary>
        /// �������� �� ������� ����� � �������
        /// </summary>
        /// <param name="key">���� �������� � ��������</param>
        /// <returns>bool</returns>
        bool HasKey(string key);

        void Load();
        void SaveServer(Action<bool> onDataSent = null);
    }
}