
using System.Collections.Generic;
using UnityEngine;

namespace AlexKadyrbayev.Plugins.DataHandler
{
    [CreateAssetMenu(menuName = "Dev/Data Handler/Data Handlers Collection")]
    public class DataHandlersCollection : ScriptableObject
    {
        [SerializeField]
        private List<DataHandlerBase> m_DataHandlers
            = new List<DataHandlerBase>();
        public List<DataHandlerBase> GetAll()
        {
            return m_DataHandlers;
        }
    }
}