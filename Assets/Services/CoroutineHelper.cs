namespace Services
{
    public static class CoroutineHelper
    {
        private static CoroutineRunner m_Runner;

        public static ICoroutineRunner GetRunner()
        {
            return m_Runner ??= new CoroutineRunner();
        }
    }
}