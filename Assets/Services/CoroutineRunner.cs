using System;
using System.Collections;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Services
{
    internal class CoroutineRunner : ICoroutineRunner, IDisposable
    {
        private static CommonMonoRunner m_CommonMonoRunner;

        public CoroutineRunner(bool dontDestroyOnLoad = false)
        {
            m_CommonMonoRunner 
                = new GameObject("BaseMonoRunner").AddComponent<CommonMonoRunner>();

            if (dontDestroyOnLoad)
            {
                Object.DontDestroyOnLoad(m_CommonMonoRunner.gameObject);
            }
        }

        public Coroutine StartCoroutine(IEnumerator enumerator)
        {
            return m_CommonMonoRunner.StartCoroutine(enumerator);
        }

        public void StopCoroutine(Coroutine coroutine)
        {
            m_CommonMonoRunner.StopCoroutine(coroutine);
        }

        public void Dispose()
        {
            Object.Destroy(m_CommonMonoRunner.gameObject);
        }
    }
}