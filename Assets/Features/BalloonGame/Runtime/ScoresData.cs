using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;

namespace Features.BalloonGame.Runtime
{
    [Serializable, JsonObject(MemberSerialization.Fields)]
    public class ScoresData
    {
        [SerializeField] private List<ScoresUserInfo> m_UsersInfos;

        public List<ScoresUserInfo> UsersInfos => m_UsersInfos;

        public void SaveUserInfo(ScoresUserInfo info)
        {
            m_UsersInfos ??= new List<ScoresUserInfo>();

            m_UsersInfos.Add(info);
        }
    }
}
