using UnityEngine;

namespace Features.BalloonGame.Runtime
{
    [CreateAssetMenu(menuName = "Dev/LifeModel")]
    public class LifeModel : ScriptableObject
    {
        [SerializeField] private int m_StartedLifes;

        public int StartedLifes => m_StartedLifes;
    }
}
