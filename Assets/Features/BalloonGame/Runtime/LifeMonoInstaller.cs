using UnityEngine;
using Zenject;

namespace Features.BalloonGame.Runtime
{
    public class LifeMonoInstaller : MonoInstaller
    {
        [SerializeField] private LifeView m_LifeView;
        
        public override void InstallBindings()
        {
            Container
                .Bind<ILifePresenter>()
                .To<LifePresenter>()
                .AsSingle()
                .NonLazy();

            Container
                .Bind<LifeView>()
                .FromInstance(m_LifeView)
                .AsSingle()
                .NonLazy();
        }
    }
}
