using UnityEngine;
using Zenject;

namespace Features.BalloonGame.Runtime
{
    public class ScoresMonoInstaller : MonoInstaller
    {
        [SerializeField] private ScoresView m_ScoresView;
        
        public override void InstallBindings()
        {
            Container.Bind<IScoresPresenter>()
                .To<ScoresPresenter>()
                .AsSingle()
                .NonLazy();

            Container
                .Bind<ScoresView>()
                .FromInstance(m_ScoresView)
                .AsSingle()
                .Lazy();
        }
    }
}
