namespace Features.BalloonGame.Runtime
{
    public interface ILifePresenter
    {
        public void Initialize();
        public void DecreaseLives();
    }
}
