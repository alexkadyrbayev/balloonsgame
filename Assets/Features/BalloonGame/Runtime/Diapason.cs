using System;
using UnityEngine;

namespace Features.BalloonGame.Runtime
{
    [Serializable]
    public struct Diapason<T>
    {
        [SerializeField] private T m_MinValue;
        [SerializeField] private T m_MaxValue;

        public T MinValue => m_MinValue;
        public T MaxValue => m_MaxValue;
    }
}