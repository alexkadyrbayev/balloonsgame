using UnityEngine;
using Zenject;

namespace Features.BalloonGame.Runtime
{
    public class LifePresenter : ILifePresenter
    {
        [Inject] private LifeView m_LifeView;
        [Inject] private IFinishScreenPresenter m_FinishScreenPresenter;

        private LifeModel m_LifeModel;
        private int m_StartedLives;
        
        public void Initialize()
        {
            m_LifeModel = GetLifeModel();
            m_StartedLives = m_LifeModel.StartedLifes;
            
            m_LifeView.UpdateView(m_StartedLives);
        }

        public void DecreaseLives()
        {
            m_StartedLives--;
            if (m_StartedLives <= 0)
            {
                m_FinishScreenPresenter.OpenView();
                return;
            }
            m_LifeView.UpdateView(m_StartedLives);
        }

        private LifeModel GetLifeModel()
        {
            var lifeModel = Resources.Load<LifeModel>("LifeModel");

            return lifeModel;
        }
    }
}
