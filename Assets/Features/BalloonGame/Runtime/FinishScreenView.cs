using System;
using UnityEngine;
using UnityEngine.UI;

namespace Features.BalloonGame.Runtime
{
    public class FinishScreenView : MonoBehaviour
    {
        [SerializeField] private InputField m_InputUsernameField;
        [SerializeField] private Button m_RestartButton;
        [SerializeField] private Text m_ErrorText;
        [SerializeField] private Text m_ScoresText;

        public event Action OnRestartButtonClickAction;
    
        public InputField InputUsernameField => m_InputUsernameField;
    
        private void Start()
        {
            m_RestartButton.onClick.AddListener(OnRestartButtonClicked);
        }

        private void OnRestartButtonClicked()
        {
            OnRestartButtonClickAction?.Invoke();
        }

        public void EnableErrorText()
        {
            m_ErrorText.gameObject.SetActive(true);
        }

        public void SetScoresText(int count)
        {
            m_ScoresText.text = count.ToString();
        }
    }
}
