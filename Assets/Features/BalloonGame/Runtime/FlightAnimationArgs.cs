using BalloonGame.Features;
using UnityEngine;

namespace Features.BalloonGame.Runtime
{
    public class FlightAnimationArgs
    {
        public BalloonView View { get; set; }
        public BalloonsModel Model { get; set; }
        public Transform EndFlightPoint { get; set; }
    }
}
