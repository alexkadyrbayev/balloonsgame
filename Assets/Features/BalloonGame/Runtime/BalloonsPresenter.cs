using System;
using BalloonGame.Features;
using DG.Tweening;
using Services.ObjectPool;
using UnityEngine;
using Zenject;
using Random = UnityEngine.Random;

namespace Features.BalloonGame.Runtime
{
    public class BalloonsPresenter : IBalloonsPresenter
    {
        [Inject] private ILifePresenter m_LifePresenter;
        [Inject] private IScoresPresenter m_ScoresPresenter;
        
        private ObjectPoolManager<BalloonView> m_BalloonsPool;
        private BalloonsModel m_Model;
        private Transform m_UpBalloonsBorder;
        private Transform m_DownBalloonsBorder;
        
        public void Configure(BalloonsPresenterConfig config)
        {
            m_BalloonsPool = config.BalloonsPool;
            m_Model = config.BalloonsModel;
            m_DownBalloonsBorder = config.DownBalloonsBorder;
            m_UpBalloonsBorder = config.UpBalloonsBorder;
            
            Initialize();
        }

        private void Initialize()
        {
            ConfigureBalloons();
        }

        private void ConfigureBalloons()
        {
            for (int i = 0; i < m_Model.PoolCount; i++)
            {
                var randomX = Random.Range(m_Model.RandomBalloonsPositionDiapason.MinValue,
                    m_Model.RandomBalloonsPositionDiapason.MinValue);
                var position = new Vector3(randomX, m_DownBalloonsBorder.position.y);
                var balloonView = m_BalloonsPool.GetFreeElement();
                
                balloonView.Initialize();

                balloonView.SetPosition(position);
                
                balloonView.OnBalloonClickAction += OnBalloonClicked;
                
                StartBalloonFlight(balloonView);
            }
        }

        private void OnBalloonFlightCompleted(BalloonView balloonView)
        {
            balloonView.KillAnimation();
            ResetBalloonView(balloonView);
        }

        private void OnBalloonClicked(BalloonView balloonView)
        {
            balloonView.KillAnimation();
            m_ScoresPresenter.IncreaseScore();
            
            ResetBalloonView(balloonView);
        }
        
        private void StartBalloonFlight(BalloonView balloonView)
        {
            if (!balloonView.gameObject.activeInHierarchy)
            {
                balloonView.Enable();
            }
            
            balloonView.CreateFlightAnimation(new FlightAnimationArgs()
            {
                Model = m_Model,
                EndFlightPoint = m_UpBalloonsBorder
            });
        }

        private void DecreaseLives()
        {
            m_LifePresenter.DecreaseLives();
        }
        
        private void ResetBalloonView(BalloonView balloonView)
        {
            balloonView.Disable();
            balloonView.SetPosition(m_DownBalloonsBorder.transform.position);
            StartBalloonFlight(balloonView);
        }
    }
}