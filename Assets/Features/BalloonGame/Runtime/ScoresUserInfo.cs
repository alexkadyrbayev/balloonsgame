using System;
using Newtonsoft.Json;
using UnityEngine;

namespace Features.BalloonGame.Runtime
{
    [Serializable, JsonObject(MemberSerialization.Fields)]
    public class ScoresUserInfo
    {
        [SerializeField] private string m_Username;
        [SerializeField] private int m_Count;

        public string Username => m_Username;
        public int Count => m_Count;

        public ScoresUserInfo(string username, int count)
        {
            m_Username = username;
            m_Count = count;
        }
    }
}
