using UnityEngine;
using UnityEngine.UI;

namespace Features.BalloonGame.Runtime
{
    public class LifeView : MonoBehaviour
    {
        [SerializeField] private Text m_LifeView;

        public void UpdateView(int count)
        {
            m_LifeView.text = count.ToString();
        }
    }
}
