using System;
using DG.Tweening;
using Features.BalloonGame.Runtime;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace BalloonGame.Features
{
    public class BalloonView : MonoBehaviour
    {
        [SerializeField] private Transform m_Balloon;
        [SerializeField] private Transform m_Robe;
        [SerializeField] private SpriteRenderer m_BalloonIcon;

        private FlightAnimationController m_FlightAnimationController;

        public Transform BalloonTransform => m_Balloon;
        public Transform RobeTransform => m_Robe;
        public SpriteRenderer BalloonIcon => m_BalloonIcon;
        
        public event Action<BalloonView> OnFlightCompleted;
        public event Action<BalloonView> OnBalloonClickAction;

        public void Initialize()
        {
            m_FlightAnimationController = new FlightAnimationController();
            SetBalloonRandomColor();
        }

        private void SetBalloonRandomColor()
        {
            var r = Random.Range(0, 1f);
            var g = Random.Range(0, 1f);
            var b = Random.Range(0, 1f);

            m_BalloonIcon.color = new Color(r, g, b);
        }

        private void OnMouseDown()
        {
            OnBalloonClicked();
        }

        private void OnBalloonClicked()
        {
            OnBalloonClickAction?.Invoke(this);
        }

        public void Disable()
        {
            gameObject.SetActive(false);
        }

        public void Enable()
        {
            gameObject.SetActive(true);    
        }
        
        public void SetPosition(Vector3 position)
        {
            gameObject.transform.position = position;
        }

        public void CreateFlightAnimation(FlightAnimationArgs args)
        {
            m_FlightAnimationController.CreateFlightTween(new FlightAnimationArgs()
            {
                Model = args.Model,
                View = this,
                EndFlightPoint = args.EndFlightPoint
            });

            m_FlightAnimationController.FlightTween.OnComplete(() => OnFlightCompleted?.Invoke(this));
        }

        public void KillAnimation()
        {
            m_FlightAnimationController.FlightTween?.Kill(true);
        }

        private void OnTriggerEnter2D(Collider2D col)
        {
            Debug.Log("desgfdasbvgdesbdf");
            if (col.gameObject.CompareTag("EndPoint"))
            {
                Debug.LogError("Pizda");
            }
        }
    }
}