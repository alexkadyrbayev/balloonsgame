using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Features.BalloonGame.Runtime
{
    [CreateAssetMenu(menuName = "Dev/BalloonsModel", fileName = "BalloonsModel")]
    public class BalloonsModel : ScriptableObject
    {
        [SerializeField] private int m_PoolCount;
        [SerializeField] private Diapason<int> m_RandomBalloonsPositionDiapason;
        [SerializeField] private Diapason<float> m_BallonsSpeedDiapason;
        [SerializeField] private AssetReference m_BalloonAssetReference;
        [SerializeField] private Diapason<float> m_DelayBetweenBalloonsStartDiapason;

        public int PoolCount => m_PoolCount;
        public Diapason<int> RandomBalloonsPositionDiapason => m_RandomBalloonsPositionDiapason;
        public object BalloonAddress => m_BalloonAssetReference.RuntimeKey;
        public Diapason<float> BalloonsSpeedDiapason => m_BallonsSpeedDiapason;
        public Diapason<float> DelayBetweenBalloonsStartDiapason => m_DelayBetweenBalloonsStartDiapason;
    }
}