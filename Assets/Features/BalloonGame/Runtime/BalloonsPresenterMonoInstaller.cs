using Zenject;

namespace Features.BalloonGame.Runtime
{
    public class BalloonsPresenterMonoInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.BindInterfacesTo<BalloonsPresenter>().AsSingle();
        }
    }
}
