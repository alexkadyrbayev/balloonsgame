using UnityEngine;
using UnityEngine.UI;

namespace Features.BalloonGame.Runtime
{
    public class ScoresView : MonoBehaviour
    {
        [SerializeField] private Text m_ScoresText;

        public void UpdateView(int scores)
        {
            m_ScoresText.text = scores.ToString();
        }
    }
}