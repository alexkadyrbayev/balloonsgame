using BalloonGame.Features;
using Services;
using Services.ObjectPool;
using UnityEngine;
using Zenject;

namespace Features.BalloonGame.Runtime
{
    public class BalloonsGameController : MonoBehaviour
    {
        [SerializeField] private BalloonView m_BalloonViewPrefab;
        [SerializeField] private Transform m_DownBalloonBorder;
        [SerializeField] private Transform m_UpBalloonBorder;

        [Inject] private IBalloonsPresenter m_BalloonsPresenter;
        
        private ObjectPoolManager<BalloonView> m_BalloonsPool;
        private object m_BalloonAddress;
        private BalloonsModel m_Model;

        private const string m_ModelKey = "BalloonsModel";

        private void Awake()
        {
            m_Model = GetBalloonModel();
            m_BalloonAddress = m_Model.BalloonAddress;

            m_BalloonsPool = new ObjectPoolManager<BalloonView>(m_BalloonViewPrefab, m_Model.PoolCount, false, m_DownBalloonBorder);
            
            m_BalloonsPresenter.Configure(new BalloonsPresenterConfig()
            {
                BalloonsModel = m_Model,
                BalloonsPool = m_BalloonsPool,
                UpBalloonsBorder = m_UpBalloonBorder,
                DownBalloonsBorder = m_DownBalloonBorder
            });
        }

        private BalloonsModel GetBalloonModel()
        {
            return Resources.Load<BalloonsModel>(m_ModelKey);
        }
    }
}
