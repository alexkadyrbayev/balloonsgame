using AlexKadyrbayev.Plugins.DataHandler;
using UnityEngine;

namespace Features.BalloonGame.Runtime
{
    [CreateAssetMenu(menuName = "Dev/DataHandlers/ScoresDataHandler", fileName = "ScoresDataHandler")]
    public class ScoresDataHandler : DataHandler<ScoresData> { }
}
