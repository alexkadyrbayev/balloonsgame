using System;
using System.Collections;
using System.Collections.Generic;
using Features.BalloonGame.Runtime;
using UnityEngine;
using Zenject;

public class LifeController : MonoBehaviour
{
    [Inject] private ILifePresenter m_LifePresenter;

    private void Start()
    {
        m_LifePresenter.Initialize();
    }
}
