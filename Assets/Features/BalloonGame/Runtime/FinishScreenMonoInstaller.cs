using UnityEngine;
using Zenject;

namespace Features.BalloonGame.Runtime
{
    public class FinishScreenMonoInstaller : MonoInstaller
    {
        [SerializeField] private FinishScreenView m_FinishScreenView;
    
        public override void InstallBindings()
        {
            Container
                .Bind<IFinishScreenPresenter>()
                .To<FinishScreenPresenter>()
                .AsSingle()
                .Lazy();

            Container
                .Bind<FinishScreenView>()
                .FromInstance(m_FinishScreenView)
                .AsSingle()
                .Lazy();
        }
    }
}
