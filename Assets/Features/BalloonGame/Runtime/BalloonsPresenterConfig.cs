using BalloonGame.Features;
using Services.ObjectPool;
using UnityEngine;

namespace Features.BalloonGame.Runtime
{
    public class BalloonsPresenterConfig
    {
        public ObjectPoolManager<BalloonView> BalloonsPool { get; set; }
        public BalloonsModel BalloonsModel { get; set; }
        public Transform DownBalloonsBorder { get; set; }
        public Transform UpBalloonsBorder { get; set; }
    }
}
