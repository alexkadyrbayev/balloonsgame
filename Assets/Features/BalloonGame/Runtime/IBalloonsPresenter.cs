namespace Features.BalloonGame.Runtime
{
    public interface IBalloonsPresenter
    {
        public void Configure(BalloonsPresenterConfig config);
    }
}