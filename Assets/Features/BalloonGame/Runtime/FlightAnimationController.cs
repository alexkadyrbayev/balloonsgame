using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Features.BalloonGame.Runtime
{
    public class FlightAnimationController
    {
        public Tween FlightTween { get; private set; }

        public void CreateFlightTween(FlightAnimationArgs args)
        {
            FlightTween = Flight(args);
            FlightTween.Play();
        }
        
        private Tween Flight(FlightAnimationArgs args)
        {
            var endPoint = args.EndFlightPoint;
            var balloon = args.View.BalloonTransform;
            var robe = args.View.RobeTransform;
            
            var flyDuration = Random.Range(args.Model.BalloonsSpeedDiapason.MinValue,
                args.Model.BalloonsSpeedDiapason.MaxValue);

            var delay = Random.Range(args.Model.DelayBetweenBalloonsStartDiapason.MinValue,
                args.Model.DelayBetweenBalloonsStartDiapason.MaxValue);
            
            var randomX = Random.Range(-3f, 3f);
            
            var mainSequence = DOTween.Sequence();

            var posX = DOTween.Sequence();
            posX.Append(balloon.DOMoveX(endPoint.transform.position.x + randomX, flyDuration / 3f).SetEase(Ease.InOutSine))
                .Append(balloon.DOMoveX(endPoint.transform.position.x - randomX, flyDuration / 3f).SetEase(Ease.InOutSine))
                .Append(balloon.DOMoveX(endPoint.transform.position.x, flyDuration / 3f).SetEase(Ease.InOutSine));

            var rotate = DOTween.Sequence();
            rotate.Append(balloon.DORotate(new Vector3(0f, 0f, -3f), (flyDuration / 3f) / 2f).SetEase(Ease.InOutSine))
                .Append(balloon.DORotate(new Vector3(0f, 0f, 3f), flyDuration / 3f).SetEase(Ease.InOutSine))
                .Append(balloon.DORotate(new Vector3(0f, 0f, -3f), flyDuration / 3f).SetEase(Ease.InOutSine))
                .Append(balloon.DORotate(Vector3.zero, (flyDuration / 3f) / 2f).SetEase(Ease.InOutSine));

            var rotate2 = DOTween.Sequence();
            rotate.AppendInterval(0.2f)
                .Append(robe.DORotate(new Vector3(0f, 0f, -randomX * (-1)), (flyDuration / 3f) / 2f).SetEase(Ease.InOutSine))
                .Append(robe.DORotate(new Vector3(0f, 0f, randomX * (-1)), flyDuration / 3f).SetEase(Ease.InOutSine))
                .Append(robe.DORotate(new Vector3(0f, 0f, -randomX * (-1)), flyDuration / 3f).SetEase(Ease.InOutSine))
                .Append(robe.DORotate(Vector3.zero, (flyDuration / 3f) / 2f).SetEase(Ease.InOutSine));

            mainSequence.AppendInterval(delay)
                .Append(balloon.DOMoveY(endPoint.transform.position.y, flyDuration).SetEase(Ease.Linear).
                    OnComplete(() => FlightTween.Kill()))
                .Join(posX)
                .Join(rotate)
                .Join(rotate2);
            
            return mainSequence;
        }
    }
}
