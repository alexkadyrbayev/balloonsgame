namespace Features.BalloonGame.Runtime
{
    public interface IScoresPresenter
    {
        public int CurrentScores { get; }
        public void IncreaseScore();
    }
}
