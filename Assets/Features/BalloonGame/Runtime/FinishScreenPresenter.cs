using AlexKadyrbayev.Plugins.DataHandler;
using AlexKadyrbayev.Plugins.DataManagement;
using UnityEngine.SceneManagement;
using Zenject;

namespace Features.BalloonGame.Runtime
{
    public class FinishScreenPresenter : IFinishScreenPresenter
    {
        [Inject] private FinishScreenView m_View;
        [Inject] private IScoresPresenter m_ScoresPresenter;
        [Inject] private IDataHandlersRegistry m_DataHandlersRegistry;

        private IDataHandler<ScoresData> m_ScoresDataHandler;

        public void OpenView()
        {
            m_View.SetScoresText(m_ScoresPresenter.CurrentScores);
            m_View.gameObject.SetActive(true);
            m_View.OnRestartButtonClickAction += OnRestartButtonClick;
        }

        private void OnRestartButtonClick()
        {
            if (m_View.InputUsernameField.text == string.Empty)
            {
                m_View.EnableErrorText();
                return;
            }
            SavePlayerInfo();

            SceneManager.LoadScene("StartScene");
        }

        private void SavePlayerInfo()
        {
            m_ScoresDataHandler = m_DataHandlersRegistry.GetDataHandlerObject<ScoresData>("scores_data");
            
            m_ScoresDataHandler.Data.SaveUserInfo(new ScoresUserInfo(m_View.InputUsernameField.text,
                m_ScoresPresenter.CurrentScores));
            
            m_ScoresDataHandler.Save();
        }
    }
}
