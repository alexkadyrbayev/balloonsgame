using Zenject;

namespace Features.BalloonGame.Runtime
{
    public class ScoresPresenter : IScoresPresenter
    {
        [Inject] private ScoresView m_ScoresView;
        
        private int m_CurrentScores = 0;

        public int CurrentScores => m_CurrentScores;
        
        public void IncreaseScore()
        {
            m_CurrentScores++;
            
            m_ScoresView.UpdateView(m_CurrentScores);    
        }
    }
}
