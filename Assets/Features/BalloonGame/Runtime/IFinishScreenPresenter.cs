namespace Features.BalloonGame.Runtime
{
    public interface IFinishScreenPresenter
    {
        public void OpenView();
    }
}
