# BalloonsGame



## Getting started

Необходимо переключиться на ветку release

В самом Unity необходимо открыть стартовую сцену (StartScene)

## Структура проекта

Основной код приложения лежит в папке Assets/Features/BalloonsGame/Runtime

Проект написан с помощью архитектурного решения MVP с использованием фреймворка Zenject

Для реализации анимации полета шарика использовались шейдеры и библиотека DOTween

Сохранение данных происходит в json файл при помощи написанного мною сервиса DataHandlers(Assets/Services/DataHandler)
